<?php
/*
Plugin Name: WordPress Responsive CSS3 Pricing Tables
Plugin URI: http://wordpress.org/plugins/wrc-pricing-tables/
Version: 2.2.7
Description: Responsive pricing table plugin developed to display pricing table in a lot more professional way on different posts or pages by SHORTCODE.
Author: Realwebcare
Author URI: http://profiles.wordpress.org/moviehour/
Text Domain: wrcpt
Domain Path: /languages/
*/

/*  Copyright 2019  Realwebcare  (email : realwebcare@gmail.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as 
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/
ob_start();
define('WRCPT_PLUGIN_PATH', plugin_dir_path( __FILE__ ));
/**
 * Internationalization
 */
function wrcpt_textdomain() {
	$domain = 'wrcpt';
	$locale = apply_filters( 'plugin_locale', get_locale(), $domain );
	load_textdomain( $domain, trailingslashit( WP_LANG_DIR ) . $domain . '/' . $domain . '-' . $locale . '.mo' );
	load_plugin_textdomain( $domain, FALSE, basename( dirname( __FILE__ ) ) . '/languages/' );
}
add_action( 'init', 'wrcpt_textdomain' );
/* Add plugin action links */
function wrcpt_plugin_actions( $links ) {
	$links[] = '<a href="'.menu_page_url('wrcpt_template', false).'">'. __('Create Table','arpg') .'</a>';
	$links[] = '<a href="https://wordpress.org/support/plugin/wrc-pricing-tables" target="_blank">'. __('Support','wrcpt') .'</a>';
	return $links;
}
add_filter( 'plugin_action_links_' . plugin_basename(__FILE__), 'wrcpt_plugin_actions' );
if(is_admin()) { include ( WRCPT_PLUGIN_PATH . 'inc/admin-menu.php' ); }
function enable_pricing_package_form() {
	wp_register_script('wrcptjs', plugins_url( 'js/wrcpt-admin.js', __FILE__ ), array('jquery'), '2.2.7');
	wp_enqueue_script('wrcptjs');
	wp_enqueue_script('jquery-ui-sortable');
	wp_enqueue_script('jquery-ui-accordion');
	wp_enqueue_script('wp-color-picker');
	wp_localize_script('wrcptjs', 'wrcptajax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
	wp_enqueue_style('wp-color-picker');
	wp_enqueue_style('wrcptfront', plugins_url( 'css/wrcpt-front.css', __FILE__ ), '', '2.2.7');
	wp_enqueue_style('wrcptadmin', plugins_url( 'css/wrcpt-admin.css', __FILE__ ), '', '2.2.7');
	wp_enqueue_style('jquery-ui-style', plugins_url( 'css/jquery-accordion.css', __FILE__ ), '', '1.10.4');
}
add_action('admin_enqueue_scripts', 'enable_pricing_package_form');
function wrc_pricing_table_styles() {
	wp_register_style('wrcptfront', plugins_url( 'css/wrcpt-front.min.css', __FILE__ ), array(), '2.2.7' );
	wp_register_style('googleFonts', '//fonts.googleapis.com/css?family=Roboto+Condensed:400,700|Droid+Sans:400,700');
	wp_enqueue_style( 'wrcptfront');
	wp_enqueue_style( 'googleFonts');
}
add_action('wp_enqueue_scripts', 'wrc_pricing_table_styles');
function add_view_port() {
	echo '<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">';
}
add_action('wp_head', 'add_view_port');
function adjustBrightness($hex, $steps) {
	$steps = max(-255, min(255, $steps));
	$hex = str_replace('#', '', $hex);
	if (strlen($hex) == 3) { $hex = str_repeat(substr($hex,0,1), 2).str_repeat(substr($hex,1,1), 2).str_repeat(substr($hex,2,1), 2); }
	$r = hexdec(substr($hex,0,2));
	$g = hexdec(substr($hex,2,2));
	$b = hexdec(substr($hex,4,2));
	$r = max(0,min(255,$r + $steps));
	$g = max(0,min(255,$g + $steps));
	$b = max(0,min(255,$b + $steps));
	$r_hex = str_pad(dechex($r), 2, '0', STR_PAD_LEFT);
	$g_hex = str_pad(dechex($g), 2, '0', STR_PAD_LEFT);
	$b_hex = str_pad(dechex($b), 2, '0', STR_PAD_LEFT);
	return '#'.$r_hex.$g_hex.$b_hex;
}
function wrc_pricing_table_shortcode( $atts, $content = null ) {
	extract( shortcode_atts( array(
		'id' => 1
	), $atts, 'wrc-pricing-table' ) );
	ob_start();
	$f_value = $f_tips = '';
	$total_feature = $flag = 0;
	$pricing_table_lists = get_option('packageTables');
	$pricing_id_lists = get_option('packageIDs');
	$pricing_table_lists = explode(', ', $pricing_table_lists);
	$pricing_id_lists = explode(', ', $pricing_id_lists);
	$key = array_search($id, $pricing_id_lists);
	if(in_array($id, $pricing_id_lists)) $flag = 1;
	$pricing_table = $pricing_table_lists[$key];
	$tableID = strtolower($pricing_table) . '-' .$id;
	$package_feature = get_option($pricing_table.'_feature');
	$packageCombine = get_option($pricing_table.'_option');
	if($package_feature) { $total_feature = count($package_feature)/2; }
	$package_lists = get_option($pricing_table);
	$packageOptions = explode(', ', $package_lists);
	$package_count = count($packageOptions);
	require_once ( WRCPT_PLUGIN_PATH . 'lib/process-shortcode.php' );
	echo wrcpt_shortcode($pricing_table, $tableID, $package_feature, $packageCombine, $total_feature, $package_lists, $packageOptions, $package_count, $flag);
	return ob_get_clean();
}
add_shortcode('wrc-pricing-table', 'wrc_pricing_table_shortcode');
?>